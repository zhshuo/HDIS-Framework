package tech.hdis.framework.push.aliyun;


import lombok.NonNull;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * 数据字典工具
 *
 * @author 黄志文
 */
@Component
public class AliyunPushDic {

    private static Environment environment;

    /**
     * 获取数据字典
     *
     * @param key key
     * @return 值
     */
    public static String get(@NonNull String key) {
        String value = environment.getProperty(key);
        if (StringUtils.isBlank(value)) {
            throw new NullPointerException("There is no value for this key:" + key + " in the properties file.");
        }
        return value;
    }

    /**
     * 获取数据字典
     *
     * @param key key
     * @return 值
     */
    public static Long getLone(@NonNull String key) {
        return Long.parseLong(get(key));
    }

    @Autowired
    public void setEnvironment(Environment environment) {
        AliyunPushDic.environment = environment;
    }
}
