package tech.hdis.framework.push.aliyun;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.push.model.v20160801.PushResponse;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.hdis.framework.push.Push;

/**
 * 阿里云推送实现
 *
 * @author 黄志文
 */
@Component
public class AliyunPushImpl implements Push {

    @Autowired
    private DefaultAcsClient client;

    /**
     * 消息推送
     *
     * @param content 推送内容
     * @param target  推送目标
     * @throws ClientException 推送异常
     */
    @Override
    public Boolean push(@NonNull String content, @NonNull String... target) throws ClientException {
        CustomPushRequest customPushRequest = CustomPushRequest.getInstance().targetValue(target).body(content);
        PushResponse response = client.getAcsResponse(customPushRequest);
        return true;
    }

    /**
     * 全体消息推送
     *
     * @param content 推送内容
     * @throws Exception 推送异常
     */
    @Override
    public Boolean pushAll(@NonNull String content) throws Exception {
        CustomPushRequest customPushRequest = CustomPushRequest.getInstance().targetValue(CustomPushRequest.TARGET_VALUE_TYPE_ALL).body(content);
        PushResponse response = client.getAcsResponse(customPushRequest);
        return true;
    }

    /**
     * 得到推送认证信息<br>
     * 用于传递给客户端进行推送信息认证<br>
     *
     * @return 推送认证信息
     */
    public Object getPushAuthenticationInfo() {
        // TODO: 2018/9/25 阿里云认证返回接口，客户端使用。
        return null;
    }

    /**
     * 消息推送
     *
     * @param customPushRequest 阿里云PushReques建造者
     * @return 是否成功
     * @throws ClientException 推送异常
     */
    public Boolean push(@NonNull CustomPushRequest customPushRequest) throws ClientException {
        PushResponse response = client.getAcsResponse(customPushRequest);
        return true;
    }
}
