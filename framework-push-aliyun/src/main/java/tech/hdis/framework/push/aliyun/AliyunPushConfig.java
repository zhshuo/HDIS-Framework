package tech.hdis.framework.push.aliyun;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 推送配置
 *
 * @author 黄志文
 */
@Configuration
public class AliyunPushConfig {

    @Autowired
    private AliyunPushProperties aliyunPushProperties;

    /**
     * 推送目标类型：ALL
     */
    public static final String REGION_ID = "cn-hangzhou";

    /**
     * 注册阿里云推送
     *
     * @return 阿里云推送
     */
    @Bean
    public DefaultAcsClient aliyunClient() {
        IClientProfile profile = DefaultProfile.getProfile(REGION_ID, aliyunPushProperties.getAccessKeyId(), aliyunPushProperties.getAccessKeySecret());
        DefaultAcsClient client = new DefaultAcsClient(profile);
        return client;
    }
}
