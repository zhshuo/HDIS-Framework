package tech.hdis.framework.push.aliyun;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * 阿里云推送参数配置
 *
 * @author 黄志文
 */
@Getter
@Setter
@ToString
@Component
@Validated
@ConfigurationProperties("hdis.aliyun.push")
public class AliyunPushProperties {
    /**
     * 阿里云RAM-AccessKeyId
     */
    @NotBlank(message = "'hdis.aliyun.push.accessKey' property can not be null.please find it in your aliyun.")
    private String accessKeyId;
    /**
     * 阿里云RAM-AccessKeySecret
     */
    @NotBlank(message = "'hdis.aliyun.push.accessSecret' property can not be null.please find it in your aliyun.")
    private String accessKeySecret;
    /**
     * 阿里云推送AppKey
     */
    @NotBlank(message = "'hdis.aliyun.push.appKey' property can not be null.please find it in your aliyun.")
    private String appKey;
    /**
     * 阿里云推送消息标题
     */
    @NotBlank(message = "'hdis.aliyun.push.title' property can not be null.please custom it.")
    private String title;
}
