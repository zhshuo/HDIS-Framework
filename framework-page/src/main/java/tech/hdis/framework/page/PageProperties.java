package tech.hdis.framework.page;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 分页属性配置
 *
 * @author 黄志文
 */
@Component
@ConfigurationProperties("hdis.page")
@Getter
@Setter
@ToString
public class PageProperties {

    /**
     * 默认每页行数，静态key
     */
    public static final String PAGE_SIZE_KEY = "hdis.page.pageSize";
    /**
     * 默认每页行数，静态value
     */
    public static Integer PAGE_SIZE_VALUE;

    /**
     * 静态注入
     */
    @PostConstruct
    public void init() {
        PAGE_SIZE_VALUE = this.getPageSize();
    }

    @Autowired
    private PageProperties pageProperties;
    /**
     * 默认每页行数
     */
    public Integer pageSize = 10;
}
