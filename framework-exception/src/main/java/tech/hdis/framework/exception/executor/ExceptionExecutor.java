package tech.hdis.framework.exception.executor;


import tech.hdis.framework.exception.chain.FirstExceptionHandler;
import tech.hdis.framework.exception.chain.LastExceptionHandler;
import tech.hdis.framework.exception.handlers.BusinessExceptionHandler;
import tech.hdis.framework.exception.handlers.BindExceptionHandler;
import tech.hdis.framework.exception.handlers.SystemExceptionHandler;
import tech.hdis.framework.exception.response.ExceptionResponse;

/**
 * 异常处理责任链处理器
 *
 * @author 黄志文
 */
public class ExceptionExecutor {

    /**
     * 单例模式
     */
    private static ExceptionExecutor instance = new ExceptionExecutor();

    /**
     * 单例模式
     */
    private ExceptionExecutor() {
        initExceptionHandler();
    }

    /**
     * 单例模式
     */
    public static ExceptionExecutor getInstance() {
        return instance;
    }

    /**
     * 初始化过滤器，所有过滤器放入队列中
     *
     * @return 过滤器队列
     */
    private void initExceptionHandler() {
        FirstExceptionHandler.getInstance().setNextHandler(BindExceptionHandler.getInstance());
        BindExceptionHandler.getInstance().setNextHandler(BusinessExceptionHandler.getInstance());
        BusinessExceptionHandler.getInstance().setNextHandler(SystemExceptionHandler.getInstance());
        SystemExceptionHandler.getInstance().setNextHandler(LastExceptionHandler.getInstance());
    }

    /**
     * 异常处理器
     *
     * @param exception 异常
     * @return 返回给界面的值
     */
    public ExceptionResponse handelException(Exception exception) {
        return FirstExceptionHandler.getInstance().doHandler(exception);
    }
}
