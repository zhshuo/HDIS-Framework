package tech.hdis.framework.exception.chain;


import tech.hdis.framework.exception.response.ExceptionResponse;

/**
 * 首位占位过滤器
 *
 * @author 黄志文
 */
public class FirstExceptionHandler extends AbstractExceptionHandlerChain {

    private static FirstExceptionHandler instance = new FirstExceptionHandler();

    private FirstExceptionHandler() {
    }

    public static FirstExceptionHandler getInstance() {
        return instance;
    }

    @Override
    public ExceptionResponse doHandler(Exception excetion) {
        return this.getNextHandler().doHandler(excetion);
    }
}