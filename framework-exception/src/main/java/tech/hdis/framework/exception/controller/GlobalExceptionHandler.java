package tech.hdis.framework.exception.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import tech.hdis.framework.exception.executor.ExceptionExecutor;
import tech.hdis.framework.exception.response.ExceptionResponse;

/**
 * 全局异常处理器
 *
 * @author 黄志文
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 全局异常处理器
     *
     * @param exception 被处理的异常
     * @return 处理结果客户端返回
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    private ExceptionResponse exceptionHandler(Exception exception) {
        return ExceptionExecutor.getInstance().handelException(exception);
    }
}
