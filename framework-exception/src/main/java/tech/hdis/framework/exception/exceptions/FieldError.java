package tech.hdis.framework.exception.exceptions;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 数据验证异常
 *
 * @author 黄志文
 */
@Getter
@Setter
@ToString
public class FieldError {
    /**
     * 表单项
     */
    private String field;
    /**
     * 异常信息
     */
    private String message;
}
