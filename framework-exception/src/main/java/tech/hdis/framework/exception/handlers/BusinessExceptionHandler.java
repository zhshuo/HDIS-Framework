package tech.hdis.framework.exception.handlers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import tech.hdis.framework.exception.chain.AbstractExceptionHandlerChain;
import tech.hdis.framework.exception.exceptions.BusinessException;
import tech.hdis.framework.exception.properties.ExceptionProperties;
import tech.hdis.framework.exception.response.ExceptionResponse;

/**
 * 空指针异常处理器
 *
 * @author 黄志文
 */
public class BusinessExceptionHandler extends AbstractExceptionHandlerChain {

    private static BusinessExceptionHandler instance = new BusinessExceptionHandler();

    private BusinessExceptionHandler() {
    }

    public static BusinessExceptionHandler getInstance() {
        return instance;
    }

    private static final Logger logger = LoggerFactory.getLogger(BusinessExceptionHandler.class);
    protected static final Marker BUSINESS_EXCEPTION_MAKER = MarkerFactory.getMarker("BusinessException");

    @Override
    public ExceptionResponse doHandler(Exception exception) {
        if (exception instanceof BusinessException) {
            logger.error(BUSINESS_EXCEPTION_MAKER, exception.getMessage(), exception);
            return ExceptionResponse.getInstance().code(ExceptionProperties.BUSINESS_KEY, exception.getMessage());
        }
        return this.getNextHandler().doHandler(exception);
    }
}
