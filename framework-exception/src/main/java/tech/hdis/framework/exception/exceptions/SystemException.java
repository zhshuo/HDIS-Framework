package tech.hdis.framework.exception.exceptions;


/**
 * 系统异常：即那些无法主动判断的异常。
 *
 * @author 黄志文
 */
public class SystemException extends AbstractException {

    /**
     * 系统异常
     *
     * @param errorCode 异常代码
     * @param message   异常消息
     */
    public SystemException(String errorCode, String message) {
        super(errorCode, message);
    }

    /**
     * 系统异常
     *
     * @param errorCode 异常代码
     * @param message   异常消息
     * @param throwable 原本异常
     */
    public SystemException(String errorCode, String message, Throwable throwable) {
        super(errorCode, message, throwable);
    }
}
