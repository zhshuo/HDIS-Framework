package tech.hdis.framework.data.mongo.config;

import com.mongodb.MongoClientOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Mongodb配置
 *
 * @author 黄志文
 */
@Configuration
public class MongoConfig {
    /**
     * MongoDB客户端配置<br>
     * Mongodb超时配置<br>
     *
     * @return MongoClientOptions MongoDB客户端配置
     */
    @Bean
    public MongoClientOptions mongoOptions() {
        return MongoClientOptions.builder().maxConnectionIdleTime(60000).build();
    }
}
