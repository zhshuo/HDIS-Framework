package tech.hdis.framework.sms.aliyun;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * 阿里云推送参数配置
 *
 * @author 黄志文
 */
@Getter
@Setter
@ToString
@Component
@Validated
@ConfigurationProperties("hdis.aliyun.sms")
public class AliyunSmsProperties {
    @NotBlank(message = "'hdis.aliyun.sms.accessKey' property can not be null.please find it in your aliyun.")
    private String accessKeyId;
    @NotBlank(message = "'hdis.aliyun.sms.accessSecret' property can not be null.please find it in your aliyun.")
    private String accessKeySecret;
    @NotBlank(message = "'hdis.aliyun.sms.signName' property can not be null.please find it in your aliyun.")
    private String signName;
    @NotBlank(message = "'hdis.aliyun.sms.templateCode' property can not be null.please find it in your aliyun.")
    private String templateCode;
    @NotNull(message = "'hdis.aliyun.sms.cacheSeconds' property can not be null.please custom it.")
    private Integer cacheSeconds;
}
