package tech.hdis.framework.swagger;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;

/**
 * swagger默认配置
 *
 * @author 黄志文
 */
@Slf4j
@Configuration
@EnableSwagger2
public class SwaggerAutoConfig {

    @Autowired
    private SwaggerProperties swaggerProperties;
    @Autowired(required = false)
    private ParameterBuilder parameterBuilder;

    @Bean
    public Docket swaggerSpringfoxDocket() {
        log.info("swagger base package:{}", swaggerProperties.getBasePackage());
        log.info("swagger title:{}", swaggerProperties.getTitle());
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(swaggerProperties.getBasePackage()))
                .build()
                .apiInfo(new ApiInfoBuilder().title(swaggerProperties.getTitle()).build());
        if (parameterBuilder != null) {
            return docket.globalOperationParameters(Arrays.asList(parameterBuilder.build()));
        }
        return docket;
    }

    @Bean
    @Primary
    public SwaggerResourcesProcessor swaggerResourcesProcessor() {
        return new SwaggerResourcesProcessor();
    }

}
