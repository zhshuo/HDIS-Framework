package tech.hdis.framework.utils.security;


import lombok.NonNull;
import org.apache.commons.lang.StringUtils;

import java.security.MessageDigest;

/**
 * MD5加密工具
 *
 * @author 黄志文
 */
public class MD5Utils {

    /**
     * MD5的KEY
     */
    private final static String KEY_MD5 = "MD5";
    /**
     * 编码格式
     */
    private final static String ENCODING = "UTF-8";

    /**
     * 根据传入的参数str 返回加密后的字符串
     *
     * @param data 加密前字符串
     * @return 加密后的字符串
     * @throws Exception 错误
     * @author 黄志文
     */
    public final static String encrypt(@NonNull String data) throws Exception {
        // 验证传入的字符串
        if (StringUtils.isBlank(data)) {
            throw new NullPointerException();
        }
        // 创建具有指定算法名称的信息摘要
        MessageDigest messageDigest = MessageDigest.getInstance(KEY_MD5);
        // 使用指定的字节数组对摘要进行最后更新
        messageDigest.update(data.getBytes(ENCODING));
        // 完成摘要计算
        byte[] md = messageDigest.digest();
        // 将得到的字节数组变成字符串返回
        return HexStringUtils.byteArrayToHexString(md);
    }
}
