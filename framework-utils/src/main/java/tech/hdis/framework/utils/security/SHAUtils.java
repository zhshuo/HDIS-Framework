package tech.hdis.framework.utils.security;


import lombok.NonNull;
import org.apache.commons.lang.StringUtils;

import java.security.MessageDigest;

/**
 * SHA加密工具
 *
 * @author 黄志文
 */
public class SHAUtils {

    /**
     * 定义加密方式
     */
    private final static String KEY_SHA1 = "SHA-1";
    /**
     * 编码格式
     */
    private final static String ENCODING = "UTF-8";

    /**
     * SHA 加密
     *
     * @param data 需要加密的字符串
     * @return 加密之后的字符串
     * @throws Exception 错误
     * @author 黄志文
     */
    private static String encrypt(@NonNull String data, String key) throws Exception {
        // 验证传入的字符串
        if (StringUtils.isBlank(data)) {
            throw new NullPointerException();
        }
        // 创建具有指定算法名称的信息摘要
        MessageDigest sha = MessageDigest.getInstance(key);
        // 使用指定的字节数组对摘要进行最后更新
        sha.update(data.getBytes(ENCODING));
        // 完成摘要计算
        byte[] bytes = sha.digest();
        // 将得到的字节数组变成字符串返回
        return HexStringUtils.byteArrayToHexString(bytes);
    }

    /**
     * SHA-1 加密
     *
     * @param data 需要加密的字符串
     * @return 加密之后的字符串
     * @throws Exception 错误
     * @author 黄志文
     */
    public static String encryptSHA1(@NonNull String data) throws Exception {
        return encrypt(data, KEY_SHA1);
    }
}
