package tech.hdis.framework.utils.chain;


/**
 * 抽象责任链模式-执行器
 *
 * @author 黄志文
 */
public abstract class AbstractHandlerExecutor<R, P> {

    /**
     * 初始化过滤器，所有过滤器放入链中，形成链表。
     */
    protected abstract void initHandler();

    /**
     * 运行第一个处理器
     *
     * @param param 链表参数
     * @return 链表返回值
     */
    public abstract R handler(P param);
}
