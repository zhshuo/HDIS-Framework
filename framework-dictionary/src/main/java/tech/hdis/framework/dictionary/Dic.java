package tech.hdis.framework.dictionary;

import lombok.NonNull;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * 数据字典，static工具类。
 *
 * @author 黄志文
 */
@Component
public class Dic {

    private static Environment environment;

    @Autowired
    public void setEnvironment(Environment environment) {
        Dic.environment = environment;
    }

    /**
     * 获取数据字典
     *
     * @param key key
     * @return 值
     */
    public static String get(@NonNull String key) {
        String value = environment.getProperty(key);
        if (StringUtils.isBlank(value)) {
            throw new NullPointerException("There is no value for this key:" + key + " in the properties file.");
        }
        return value;
    }

    /**
     * 获取数据字典
     *
     * @param key key
     * @return 值
     */
    public static Long getLong(@NonNull String key) {
        return Long.valueOf(get(key));
    }

    /**
     * 获取数据字典
     *
     * @param key key
     * @return 值
     */
    public static Integer getInt(@NonNull String key) {
        return Integer.valueOf(get(key));
    }
}