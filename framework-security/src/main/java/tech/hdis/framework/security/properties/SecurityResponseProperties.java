package tech.hdis.framework.security.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 认证与授权配置
 *
 * @author 黄志文
 */
@Component
@ConfigurationProperties(value = "hdis.response.security")
@Getter
@Setter
@ToString
public class SecurityResponseProperties {
    /**
     * 默认未认证提示信息key
     */
    public static final String UNAUTHENTICATED_KEY = "hdis.response.security.unauthenticated";
    /**
     * 默认未授权提示信息key
     */
    public static final String UNAUTHORIZED_KEY = "hdis.response.security.unauthorized";

    /**
     * 默认未认证提示信息
     */
    public String unauthenticated = "请您登录";
    /**
     * 默认未授权提示信息
     */
    public String unauthorized = "您还没有权限";
}
