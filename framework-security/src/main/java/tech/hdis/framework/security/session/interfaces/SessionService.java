package tech.hdis.framework.security.session.interfaces;


import tech.hdis.framework.security.session.entity.Session;

/**
 * 基础Session管理
 *
 * @author 黄志文
 */
public interface SessionService {

    /**
     * 新增或修改session
     *
     * @param session session
     */
    void saveSession(Session session);

    /**
     * 绑定当前线程session
     *
     * @param sessionId sessionId
     */
    void bindingSession(String sessionId);

    /**
     * 刷新session，将最后访问时间与当前时间同步；
     */
    void refresh();

    /**
     * 重刷session
     * 1：判断当前session是否失效
     * 2：处理单端单用户登录（一个‘用户账号’的‘手机端’只能有一个在线）
     *
     * @return 是否刷新成功
     */
    Boolean flush();

    /**
     * 登出，需要已登录状态
     */
    void logout();

    /**
     * 获取当前线程Session
     *
     * @return Session
     */
    Session getSession();

    /**
     * session是否失效
     *
     * @return session是否失效
     */
    boolean isExpired();

    /**
     * 是否具有角色
     *
     * @param roles 角色字符串
     * @return 是否具有角色
     */
    Boolean hasRoles(String[] roles);

    /**
     * 是否具有权限
     *
     * @param permissions 权限字符串
     * @return 是否具有权限
     */
    Boolean hasPermissions(String[] permissions);
}
