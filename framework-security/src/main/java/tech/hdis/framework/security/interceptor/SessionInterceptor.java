package tech.hdis.framework.security.interceptor;

import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import tech.hdis.framework.security.annotation.SessionMark;
import tech.hdis.framework.security.properties.SecurityResponseProperties;
import tech.hdis.framework.security.response.SecurityResponse;
import tech.hdis.framework.security.session.interfaces.SessionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义权限拦截器
 * 作用于被@Session标记的方法
 * 用于处理登录，单点登录，权限的控制
 *
 * @author 黄志文
 */
@Component
public class SessionInterceptor extends HandlerInterceptorAdapter {

    // TODO: 2017/12/4 责任链模式

    private Gson gson = new Gson();

    @Autowired
    private SecurityResponseProperties securityResponseProperties;
    @Autowired
    private SessionService sessionService;

    /**
     * 设置response返回值
     *
     * @param response response
     * @param code     返回值代码
     * @author 黄志文
     */
    private void setResponse(HttpServletResponse response, String code, String message) throws IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        response.getWriter().write(gson.toJson(SecurityResponse.getInstance(code, message)));
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //强制方法类型转化检测
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        //是否使用了权限注解
        SessionMark sessionMark = ((HandlerMethod) handler).getMethodAnnotation(SessionMark.class);
        boolean hasAccess = sessionMark != null;
        //请求是否带了token
        String token = request.getParameter("token");
        boolean hasToken = StringUtils.isNotBlank(token);
        //没有权限注解
        //没有token
        if (!hasAccess && !hasToken) {
            return true;
        }
        //没有权限注解
        //有token
        if (!hasAccess && hasToken) {
            //初始化session，使用sessionService管理
            sessionService.bindingSession(token);
            return true;
        }
        //有权限注解
        //没有token
        if (hasAccess && !hasToken) {
            setResponse(response, SecurityResponseProperties.UNAUTHENTICATED_KEY, securityResponseProperties.getUnauthenticated());
            return false;
        }
        //有权限注解
        //有token
        if (hasAccess && hasToken) {
            //初始化session
            sessionService.bindingSession(token);
            //session是存在与过期
            if (sessionService.isExpired()) {
                setResponse(response, SecurityResponseProperties.UNAUTHENTICATED_KEY, securityResponseProperties.getUnauthenticated());
                return false;
            }
            //刷新session
            sessionService.refresh();
            //角色处理
            //判断当前用户是否具有所标注的字符串角色。
            if (!sessionService.hasRoles(sessionMark.roles())) {
                setResponse(response, SecurityResponseProperties.UNAUTHORIZED_KEY, securityResponseProperties.getUnauthorized());
                return false;
            }
            //权限处理
            //判断当前用户是否具有所标注的字符串权限。
            if (!sessionService.hasPermissions(sessionMark.permissions())) {
                setResponse(response, SecurityResponseProperties.UNAUTHORIZED_KEY, securityResponseProperties.getUnauthorized());
                return false;
            }
            //单点登录处理
            //当有启用单点登录的用户才执行
            if (sessionMark.isSingle()) {
                //踢掉其他用户并且判断当前session是否失效
                if (!sessionService.flush()) {
                    setResponse(response, SecurityResponseProperties.UNAUTHENTICATED_KEY, securityResponseProperties.getUnauthenticated());
                    return false;
                }
            }
            return true;
        }
        return true;
    }
}
