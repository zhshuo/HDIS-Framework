package tech.hdis.framework.push;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 消息体，提供消息路由功能
 *
 * @author 黄志文
 */
@Getter
@Setter
@ToString
public class PushBody implements Serializable {

    /**
     * 消息类型<br>
     * 消息类型为业务自定义消息类型<br>
     * 用于在业务中区分消息
     */
    private String msgType;
    /**
     * 时间戳
     */
    private Date timestamp;
    /**
     * 消息体
     */
    private Object msg;

    public PushBody() {
        this.msgType = "default";
        this.timestamp = new Date();
    }

    public static PushBody getInstance() {
        return new PushBody();
    }

    public PushBody msg(@NonNull Object message) {
        this.setMsg(message);
        return this;
    }

    public PushBody msgType(@NonNull String msgType) {
        this.setMsgType(msgType);
        return this;
    }

    public String toJson() {
        return JSON.toJSONString(this);
    }
}
